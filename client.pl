﻿###############################################################################################
# Client
###############################################################################################
#
# ALTER TABLE zwemmers ADD tydhhmm varchar(5);
# ALTER TABLE zwemmers ADD bewerk varchar(10);
#
#
#

use vars qw
	(	@velddef $myuserid $password  
		$mw $statmesg 
		$serverhost $dbname $dbd $dbh
		$sth_slct $sth_lock $sth_unlock $sth_bewerk $sth_werkaf $sth_tyden $sth_highnr
		$current_year $today $today1
	);
#
#	@velddef	Velddefinities. Zie algvars.pm
#	$myuserid	Userid van degene die is aangelogd
#	$password	Bijbehorend password
#	$mw		Main Window
#	$statmesg	Boodschappenveld. Inhoud wordt automatisch gedisplayed
#
# SQL-variabelen:
#	$serverhost	
#	$dbname 	Naam van de database
#	$dbd		Databasesoort (MySql)
#	$dbh		Databasehandle

use utf8;
use Win32;
$myuserid = Win32::LoginName;
# Windows-eigenaardigheid: 1e karakter wordt uppercase gemaakt.
$myuserid =~ tr/A-Z/a-z/;
$serverhost	= "z4d01";
#$serverhost	= "127.0.0.1";
#$serverhost	= "130.145.153.34";
#$myuserid	= "ton";

use Tk;
use DBI;
use strict;
use algvars;
use Net::Printer;
use PDF::Create;
use File::Slurper 'read_text';

my $lineprinter= new Net::Printer(
		printer => "lpr",
		server => "192.168.10.57",
		port => 515,
		lineconvert => "YES"
	);

###############################################################################################
# variables
###############################################################################################
$dbname		 = "z4d";
$password	= "";
$dbd		 = "mysql";
$current_year	= (gmtime(time))[5] + 1900;
(undef,undef,undef,my $mday,my $month) = gmtime(time);
		
$today = sprintf( "%04d%02d%02d",
		$current_year, $month + 1, $mday );
$today1 = sprintf("%02d-%02d-%04d",$mday, $month+1, $current_year);




###############################################################################################
# subroutines
###############################################################################################
sub init_db
{
	my $dsn = "DBI:mysql:$dbname:$serverhost";
	my $cmd;
	
#	$dbh = DBI->connect( $dsn, $myuserid, $password,
#				{ RaiseError => 1, AutoCommit => 0 } );
#		Dit geeft een error:
# 		Transactions not supported by database at C:/Perl/site/lib/DBI.pm line 532.
# 		Database handle destroyed without explicit disconnect.
	$dbh = DBI->connect( $dsn, $myuserid, $password, { RaiseError => 0 , mysql_enable_utf8 => 1} );
	$dbh->{mysql_auto_reconnect} = 1;
	if ( !$dbh )
	{
		my $msg = sprintf("Eerst server starten: $DBI::err .... $DBI::errstr");
		disperr( $msg );
		return 1;
	}
	
#	Prepare the statement-handles
	$cmd = "LOCK TABLES zwemmers WRITE";
	if ( ! ($sth_lock = $dbh->prepare_cached( "$cmd" ) ) )
	{	$statmesg = sprintf("Kan database niet locken???");
		printf( "Error prepare lock: %s\n", $dbh->errstr );
		return 1;
	}

	$cmd = "SELECT * FROM ZWEMMERS WHERE deelnr = ?";
	if ( ! ($sth_slct = $dbh->prepare_cached( "$cmd" ) ) )
	{	$statmesg = sprintf("Kan record niet vinden???");
		printf( "Error prepare select: %s\n", $dbh->errstr );
		return 1;
	}
	
	$cmd = "UNLOCK TABLES";
	if ( ! ($sth_unlock = $dbh->prepare_cached( "$cmd" ) ) )
	{	$statmesg = sprintf("Kan database niet unlocken???");
		printf( "Error prepare unlock: %s\n", $dbh->errstr );
		return 1;
	}

	$cmd=sprintf( "UPDATE zwemmers SET bewerk=? WHERE deelnr=?");
	if ( ! ($sth_bewerk = $dbh->prepare_cached( "$cmd" ) ) )
	{	$statmesg = sprintf("Kan record niet reserveren???");
		printf( "Error prepare bewerk: %s\n", $dbh->errstr );
		return 1;
	}

	$cmd=sprintf( "UPDATE zwemmers SET bewerk='' WHERE deelnr=?");
	if ( ! ($sth_werkaf = $dbh->prepare_cached( "$cmd" ) ) )
	{	$statmesg = sprintf("Kan record niet vrijgeven???");
		printf( "Error prepare afwerk: %s\n", $dbh->errstr );
		return 1;
	}

	$cmd = sprintf( "SELECT tydhhmm FROM zwemmers WHERE aangemeld LIKE '%s%%'",
			$current_year);
	if ( ! ($sth_tyden = $dbh->prepare_cached( "$cmd" ) ) )
	{	$statmesg = sprintf("Kan zwemtijden niet bepalen???");
		printf( "Error prepare tyden: %s\n", $dbh->errstr );
		return 1;
	}
	
	$cmd = "SELECT MAX( 1 * deelnr ) FROM zwemmers";
	if ( ! ($sth_highnr = $dbh->prepare_cached( "$cmd" ) ) )
	{	$statmesg = sprintf("Kan geen nieuw deelnemersnummer bepalen???");
		printf( "Error prepare highnr: %s\n", $dbh->errstr );
		return 1;
	}
}; # end sub init_db





sub sluit_window( $$ )
{	# Controleer of record is gewijzigd.
	# Zoja, dan vragen wat er mee moet gebeuren:
	#	- saven
	#	- wijzigingen laten vervallen
	#	- doorgaan met wijzigen
	# Indien saven of laten vervallen, dan na afloop $window sluiten
	
	my ( $window, $record ) = @_;
	
	my $top;
	my %dbrecord;
	my $cmd_ok = 1;
	
	# Als deelnr > 0, dan is het een bestaand record.
	# De velden worden dan vergeleken met de velden zoals ze nog in de database staan.
	# Als deelnr = 0, dan is het een nieuw record
	# In dat geval worden de velden vergeleken met lege velden
	# Als er dan iets in staat, dan moet het record worden gesaved.
	foreach (@velddef)
	{
		$dbrecord{$_->{key}} = "";
	}
	$dbrecord{deelnr} = $record->{deelnr};
	
	if ( $record->{deelnr} )
	{	$cmd_ok = leesrecord( \%dbrecord, $record->{deelnr} );
	}
	
	if ( $cmd_ok )
	{	# dbrecord ingelezen. Nu controleren met $record
		my $changed = 0;
		foreach (@velddef)
		{	$changed = $changed || $record->{$_->{key}} ne $dbrecord{$_->{key}};
		}
		if ( $changed )
		{	# OK, nu vragen wat er mee moet gebeuren
			$top = $mw->Toplevel();
			$top->waitVisibility;
			$top->grab();
			$top->raise();
			$top->focus();
			$top->title( "Waarschuwing" );
			my $frame = $top->Frame->pack( -fill => 'both' );
			$frame->Label
				( -text	=> "
De gegevens van deelnemer '$record->{naam}' zijn gewijzigd.
Wat wilt u met de wijzigingen doen?"
				)->pack;
			$frame->Button
				( -text		=> "Wijzigingen opslaan"
				, -command	=>
					sub	{	if ( save_record( $record ) )
							{	geef_vrij( $record->{deelnr} );
								$record->{deelnr} = NULL;
								destroy $window;
							}
							destroy $top;
						}
				)->pack
					( -expand	=> 1
					, - fill	=> 'both'
					);
			$frame->Button
				( -text		=> "Wijzigingen laten vervallen"
				, -command	=>
					sub	{	geef_vrij( $record->{deelnr} );
							$record->{deelnr} = NULL;
							destroy $window;
							destroy $top;
						}
				)->pack
					( -expand	=> 1
					, - fill	=> 'both'
					);
			$frame->Button
				( -text		=> "Doorgaan met wijzigen"
				, -command	=> sub { destroy $top }
				)->pack
					( -expand	=> 1
					, - fill	=> 'both'
					);
		} else { geef_vrij( $record->{deelnr} );
			$record->{deelnr} = NULL;
			destroy $window;
		}
	} else { destroy $window; }
}; # end sub sluit_window





sub leesrecord( $$ )
{	# Lees het DB-record met deelnr=$deelnr en vul de hash %$record daarmee
	my ( $record, $deelnr ) = @_;
	
	my @data;
	my $cmd_ok = 1;
	
	if ( ! $sth_slct->execute( $deelnr ) )
	{	$statmesg = sprintf("Kan record niet vinden???");
		printf( "Error sth_slct: %s\n", $sth_slct->errstr );
		$cmd_ok = 0;
	} else {
		# vul record (moet eerst worden gedaan zodat DBI weet
		# hoeveel records er zijn)
		@data = $sth_slct->fetchrow_array();
		if ( $sth_slct->rows == 0 )
		{
			$cmd_ok = 0;
		}
		$sth_slct->finish;
	}; # end if sth_slct->execute

	if ( $cmd_ok )
	{	# er is een record gevonden.
		# Dit moet in de hash %record worden gezet
		my $recix = 0;
		foreach (@velddef)
		{
			$record->{$_->{key}} = $data[$recix++];
		}
	}
	return $cmd_ok;
}; # end sub leesrecord





sub check4bewerk ()
{	# controleer of er records zijn die worden bewerkt door $myuserid
	
	my @deelnr;
	
	# Statementhandle preparen
	my $sth;
	my $cmd = "SELECT deelnr FROM ZWEMMERS WHERE bewerk = ?";
	if ( ! ($sth = $dbh->prepare_cached( "$cmd" ) ) )
	{	$statmesg = sprintf("Fout bij prepare check???");
		printf( "Error prepare select: %s\n", $dbh->errstr );
		return 1;
	}
	
	# Nu records lezen
	if ( ! $sth->execute( $myuserid ) )
	{	$statmesg = "Zoekcommando mislukt";
		printf( "Error sth->execute: %s\n", $sth->errstr );
		return 1;
	}
       
	# en array vullen met gevonden deelnemernummers
	while ( my @data = $sth->fetchrow_array() )
	{
		push( @deelnr, $data[0]);
	}
	$sth->finish;
	
	# Als er records zijn gevonden, dan waarschuwen
	if ( $sth->rows )
	{
		my $top = $mw->Toplevel();
		$top->waitVisibility;
		$top->grab();
		$top->raise();
		$top->focus();
		$top->title( "Waarschuwing" );
		my $frame = $top->Frame->pack( -fill => 'both' );
		$frame->Label
			( -text	=> "
Userid '$myuserid' heeft nog records in bewerking
Dit kan de volgende oorzaken hebben:
"
			, -relief	=> 'flat'
			, -justify	=> 'left'
			)->pack();
		$frame->Button
			( -text	=>
"Een vorige sessie is foutief afgesloten.
Druk hier om de records weer beschikbaar te maken."
#			, -relief => 'ridge'
			, -command	=> sub
					{	geef_vrij( @deelnr );
						destroy $top;
					}
			)->pack	( -side		=> 'top'
				, -expand	=> 1
				, -fill		=> 'x'
				);
		$frame->Button
			( -text	=>
"U heeft nog ergens een andere sessie lopen.
Druk hier om de records te laten zoals ze zijn."
#			, -relief 	=> 'ridge'
			, -command	=> sub { destroy $top; }
			)->pack	( -side		=> 'top'
				, -expand	=> 1
				, -fill		=> 'x'
				);
		$frame->Button
			( -text	=>
"Er is iets anders aan de hand.
Druk hier om deze sessie af te sluiten."
#			, -relief => 'ridge'
			, -command	=> sub { destroy $mw; }
			)->pack	( -side		=> 'top'
				, -expand	=> 1
				, -fill		=> 'x'
				);
	}; # einde if $sth->rows
}; # end sub check4bewerk





sub create_search ( $ )
{
	# Vul search-frame
	# Parameters: $frame = search-frame
	(my $srcframe) = @_;

	#
	# uses global variables: @velddef
	# velddef is een array met pointers naar de velddefinities
	# dus $velddef->key = key in database
	#  en $velddef->txt = de tekst die op het scherm moet komen
	#  enz.
	#
	# Deze subroutine heeft alleen de tekst nodig.
	# Elk veld wordt een normaal stringveld.

	my $fld;
	my %srchrec;

	my $listbox = $srcframe->Scrolled
		( "Listbox"
		, -scrollbars	=> 'osoe'
		)->pack
			( -side		=> 'right'
			, -fill		=> 'both'
			, -expand	=> 1
			);
	$listbox->bind( '<Double-1>' =>
			sub {	my ($key) = split( '\s+', $_[0]->get('active') );
				edit_or_display( $key );
			}
		);
	my $frame = $srcframe->Frame()->pack( -side => 'top' );
	
	# definieer listbox
	
	$statmesg = "Z O E K - V E N S T E R";

	foreach (@velddef)
	{
		$srchrec{$_->{key}} = "";
	}

	my $zoekbutton = $srcframe->Button
		( -text		=> "Zoek"
		, -command	=> sub
			{ vul_listbox( $listbox, \%srchrec ); }
		)-> pack
			( -side		=> 'left'
			, -expand	=> 1
			, -fill		=> 'x'
			);
	$srcframe->Button
		( -text	=> "Nieuw record"
		, -command	=> \&nieuw_record
		)->pack	( -side		=> "left"
			, -expand	=> 1
			, -fill		=> 'x'
			);
	$srcframe->Button
		( -text	=> "Einde"
		, -command	=> sub { destroy $mw }
		)->pack	( -side 	=> "right"
			, -expand	=> 1
			, -fill		=> 'x'
			);
	disp_record( $frame, \%srchrec, 2, $zoekbutton );
	
}; # end sub create_search





sub vul_listbox( $$ )
{
	my ($listbox, $srchrec) = @_;
	
	my $cmd = "SELECT * FROM ZWEMMERS";
	my $where_and = "WHERE";
	my @qry_parms = ();
	my $sth; # Statement-handle
	
	foreach (@velddef)
	{	# Als 'n veld gevuld is, dan opnemen in query
		if ( $srchrec->{$_->{key}} )
		{
			$cmd = sprintf( "%s %s %s LIKE ?",
				$cmd, $where_and, $_->{key} );
			$where_and = "AND";
			push( @qry_parms, "%" . $srchrec->{$_->{key}} . "%");
		}
	}
	if ( ! ($sth = $dbh->prepare_cached( "$cmd" ) ) )
	{	$statmesg = sprintf("Zoekcommando mislukt");
		printf( "Error prepare_cached: %s\n",
				$dbh->errstr );
		return 1;
	}
	#
	# Nu kan de listbox worden gevuld.
	#
	if ( ! $sth->execute( @qry_parms ) )
	{	$statmesg = "Zoekcommando mislukt";
		printf( "Error sth->execute: %s\n", $sth->errstr );
		return 1;
	}
       
	$listbox->delete( 0, 'end' );

	while ( my @data = $sth->fetchrow_array() )
	{
		my $deelnr = $data[0];
		my $naam   = $data[1];
		my $lb_val = sprintf( "%d %s",$deelnr, $naam );
		$listbox->insert( 'end', $lb_val );
	}
	$statmesg = sprintf( "Aantal gevonden records: %d", $sth->rows );
	$sth->finish;
}; # end sub vul_listbox



sub geef_vrij( @ )
{
	foreach my $deelnr (@_)
	{
		if ( ! $sth_werkaf->execute( $deelnr ) )
		{	$statmesg = sprintf( "Kan deelnemer %s niet vrijgeven???", $deelnr );
		}
	}
}; # end sub geef_vrij





sub nieuw_record ( )
{	# Er wordt een leeg record gemaakt. Geen parameters
	
	my $deelnr;
	my %record;
	my $top;
	my $frame;

	# Start een nieuw record gevonden.
	foreach (@velddef)
	{
		$record{$_->{key}} = "";
	}
	$record{deelnr} = 0;
	$record{woonpl} = "Valkenswaard";
	$record{medail} = 0;
	$record{medalsrt} = 'Normaal';
	$record{land} = 'Nederland';
	$record{bewerk} = $myuserid;
	
	$top = $mw->Toplevel();
	$top->waitVisibility;
	$top->Label
		( -textvariable	=> \$statmesg
		)->pack	( -side		=> 'top'
			, -fill		=> 'x'
			, -expand	=> 0
			);
	$statmesg = "Nieuw Record";
	$top->protocol
		(	"WM_DELETE_WINDOW", 
			sub { sluit_window( $top, \%record ); }
		);

	$top->title( "Nieuw record" );
	$frame = $top->Frame()->pack();
	$top->Button
		( -text		=> "Opslaan"
		, -command	=> sub
				{	if ( save_record( \%record ) )
					{
						$deelnr = $record{deelnr};
						geef_vrij( $deelnr );
						my $geom = $top->geometry();
						destroy $top;
						edit_or_display( $deelnr, $geom );
					}
				}
		)->pack	( -side => "left"
			, -expand	=> 1
			, -fill		=> 'x'
			);
	$top->Button
		( -text		=> "Plan in"
		, -command	=> [ sub {inplannen(\%record);} ]
		)->pack	( -side => "left"
			, -expand	=> 1
			, -fill		=> 'x'
			);
	$top->Button
		( -text		=> "Sluit"
		, -command	=> sub { sluit_window( $top, \%record  ); }
		)->pack	( -side => "right"
			, -expand	=> 1
			, -fill		=> 'x'
			);

	disp_record( $frame, \%record, 1 );
}; #END sub nieuw_record






sub edit_or_display ( $@ )
{	# Het record met opgegeven deelnr wordt op scherm gezet.
	# Indien nog niemand aan het wijzigen is, wordt record gelockt en kan men wijzigen.
	# Anders wordt record slechts ter informatie op scherm gezet.
	my ($deelnr, $geom) = @_;
	
	my %record;
	my @data;
	my $editen = 0;
	my $title = '';
	my $cmd_ok = 1;
	my $top;
	my $frame;

#	execute the statements
	# Nu database locken
	if ( ! $sth_lock->execute )
	{	$statmesg = sprintf("Kan database niet locken???");
		printf( "Error sth_lock: %s\n", $sth_lock->errstr );
		return 1;
	}
	
	# Zoek het record
	$cmd_ok = leesrecord( \%record, $deelnr );

	if ( $cmd_ok )
	{	# er is een record gevonden.
		# nu kijken of er gewijzigd mag worden
		$editen = 0;
		$title = "BEKIJKEN";
		if ( ! $record{bewerk} )
		{
			if ( ! $sth_bewerk->execute( $myuserid, $deelnr ) )
			{
				$statmesg = sprintf("Kan record niet reserveren???");
				printf( "Error sth_bewerk: %s\n", $sth_bewerk->errstr );
			} else {
				$editen = 1;
				$title = "WIJZIGEN";
				$record{bewerk} = $myuserid;
			}
		}
	}

	if ( ! $sth_unlock->execute() )
	{	$statmesg = sprintf("Kan database niet unlocken???");
		printf( "Error sth_unlock: %s\n", $sth_unlock->errstr );
	}

	if ( ! $cmd_ok )
	{	$statmesg = sprintf( "Kan deelnemer %d niet vinden", $deelnr);
		return 1;
	}

	$top = $mw->Toplevel();
	$top->waitVisibility;
	$top->Label
		( -textvariable	=> \$statmesg
		)->pack	( -side		=> 'top'
			, -fill		=> 'x'
			, -expand	=> 0
			);
	$statmesg = sprintf( "%s", $title );

	$top->title( "$title: $record{naam}" );
	if ( $geom )
	{
		$top->geometry( $geom );
	}
	$frame = $top->Frame()->pack();
	$top->Button
		( -text	=> "Kloon"
		, -command	=> sub { kloon( $deelnr ) }
		)->pack( -side => "left" );
	if ( $editen )
	{	$top->protocol
			(	"WM_DELETE_WINDOW", 
				sub { sluit_window( $top, \%record ); }
			);
		$top->Button
			( -text		=> "Opslaan"
			, -command	=> sub { save_record( \%record ) }
			)->pack	( -side => "left"
				, -expand	=> 1
				, -fill		=> 'x'
				);
		$top->Button
			( -text		=> "Plan in"
			, -command	=> [ sub {inplannen(\%record);} ]
			)->pack	( -side => "left"
				, -expand	=> 1
				, -fill		=> 'x'
				);
		$top->Button
			( -text		=> "Print kaart"
			, -command	=> sub { printkaart( $top, \%record ); }
			)->pack( -side => 'left'
				, -expand	=> 1
				, -fill		=> 'x'
				);
		$top->Button
			( -text		=> "Sluit"
			, -command	=> sub { sluit_window( $top, \%record  ); }
			)->pack( -side => "right"
				, -expand	=> 1
				, -fill		=> 'x'
				);
	} else {
		$top->Button
			( -text		=> "Wijzig"
			, -command	=> sub
					{	my $geom = $top->geometry();
						destroy $top;
						edit_or_display( $deelnr, $geom );
					}
			)->pack( -side => "left"
				, -expand	=> 1
				, -fill		=> 'x'
				);
		$top->Button
			( -text		=> "Print kaart"
			, -command	=> sub { printkaart( $top, \%record ); }
			)->pack( -side => 'left'
				, -expand	=> 1
				, -fill		=> 'x'
				);
		$top->Button
			( -text		=> "Sluit"
			, -command	=> sub { destroy $top; }
			)->pack( -side => "right"
				, -expand	=> 1
				, -fill		=> 'x'
				);
	}
	
		
	disp_record( $frame, \%record, $editen );
}; #END sub edit_or_display





sub kloon ( $ )
{	# Er wordt een kopie gemaakt van het record met opgegeven deelnr.
	my ($deelnr) = @_;
	
	my %record;
	my $top;
	my $frame;

#	execute the statements
	# Zoek het record
	if ( ! leesrecord( \%record, $deelnr ) )
	{
		$statmesg = sprintf( "Kan deelnemer %d niet vinden", $deelnr );
	}

	# er is een record gevonden.
	$record{deelnr} = 0;
	$record{birth} = '0000-00-00';
	$record{aangemeld} = '';
	$record{medail} = 0;
	$record{medalsrt} = 'Normaal';
	$record{tydhhmm} = '';
	$record{bewerk} = $myuserid;
	
	$top = $mw->Toplevel();
	$top->waitVisibility;
	$top->Label
		( -textvariable	=> \$statmesg
		)->pack	( -side		=> 'top'
			, -fill		=> 'x'
			, -expand	=> 0
			);
	$statmesg = "Nieuw Record";
	$top->protocol
		(	"WM_DELETE_WINDOW", 
			sub { sluit_window( $top, \%record ); }
		);

	$top->title( "Kloon: $record{naam}" );
	$frame = $top->Frame()->pack();
	$top->Button
		( -text		=> "Opslaan"
		, -command	=> sub
				{	if ( save_record( \%record ) )
					{
						my $geom = $top->geometry();
						$deelnr = $record{deelnr};
						geef_vrij( $deelnr );
						destroy $top;
						edit_or_display( $deelnr, $geom );
					}
				}
		)->pack	( -side => "left"
			, -expand	=> 1
			, -fill		=> 'x'
			);
	$top->Button
		( -text		=> "Plan in"
		, -command	=> [ sub {inplannen(\%record);} ]
		)->pack	( -side		=> "left"
			, -expand	=> 1
			, -fill		=> 'x'
			);

	disp_record( $frame, \%record, 1 );
	$top->Button
		( -text		=> "Sluit"
		, -command	=> sub { sluit_window( $top, \%record  ); }
		)->pack
			( -side		=> "right"
			, -fill		=> 'x'
			, -expand	=> 1
			);
}; #END sub kloon






sub inplannen( $ )
{
	my ($record) = @_;
	# Men wil weten hoeveel mensen er per tijdseenheid aan het zwemmen zijn
	# Daarom krijgen ze het overzicht zodra iemand wordt ingepland.
	#
	my %tydschema;
	
	for ( my $hh = 15; $hh < 20; $hh++ )
	{	for ( my $mm = 0; $mm < 60; $mm += 20 )
		{
			my $tyd = sprintf( "%02d:%02d", $hh, $mm );
			$tydschema{$tyd} = 0;
		}
	}
	if ( ! $sth_tyden->execute() )
	{	$statmesg = sprintf("Kan zwemtijden niet bepalen???");
		printf( "Error sth_tyden: %s\n", $sth_tyden->errstr );
		return 1;
	}
	
	# Statement geeft enkel het veld tydhhmm terug.
	while ( my @data = $sth_tyden->fetchrow_array() )
	{
		$tydschema{$data[0]} ++;
	}
	$sth_tyden->finish;

	my $top = $mw->Toplevel();
	$top->waitVisibility;
	$top->Label( -text => "Inplannen" )->pack;
	$top->title( "Inplannen $record->{naam}" );
	$top->grab();
	
	my $listbox = $top->Scrolled
		( "Listbox"
		, -scrollbars	=> 'osoe'
		, -height	=> 17
		)->pack	( -side		=> 'right'
			, -fill		=> 'both'
			, -expand	=> 1
			);
	$listbox->bind	( '<Double-1>' =>
			  sub {	plan_zwemtyd( $top, $record, $_[0]->get('active') ) }
			);
	
	for ( my $hh = 15; $hh < 20; $hh++ )
	{	for ( my $mm = 0; $mm < 60; $mm += 20 )
		{
			my $tyd = sprintf( "%02d:%02d", $hh, $mm );
			my $regel = sprintf( "%s = %d", $tyd, $tydschema{$tyd} );
			$listbox->insert( 'end', $regel );
		}
	}
}; # end sub inplannen





sub plan_zwemtyd( $$$ )
{
	my ($top, $record, $tyd) = @_;
	
	($tyd) = split( '\s+', $tyd );
	$record->{tydhhmm} = $tyd;
	if ( $current_year != substr( $record->{aangemeld}, 0, 4 ) )
	{
		$record->{aangemeld} = $today;
		$record->{medail} ++;
	}
	destroy $top;
}; # end sub plan_zwemtyd





sub check_record ( $ )
{
	my ($record) = @_;
	
	my $alles_ok = 1;
	my $ix = "";

	foreach $ix (@velddef)
	{
		if ( &{$ix->{check}}($record) == 0 )
		{
			$alles_ok = 0;
			$statmesg = sprintf( "Veld '%s' niet juist ingevuld",
					$ix->{txt} );
		}
	}
	
	return $alles_ok;
}; # end sub check_record







sub save_record ( $ )
{
	my ($record) = @_;
	my $success = 0;
	my $recin   = "";
	my @fmtfields = ();
	my $cmd = "";
	my $sth;
	my $deelnr = $record->{deelnr};

	$statmesg = sprintf( "Controleren velden" ); $mw->update;
	if ( ! check_record ( $record ) )
	{ # check_record vult $statmesg met juiste boodschap
		return 0;
	}

	if ( $deelnr == 0 )
	{
		$cmd = "select MAX( 1 * deelnr ) from zwemmers";
		$sth_highnr = $dbh->prepare( $cmd);
		$sth_highnr->execute;
		my @TMPARR = $sth_highnr->fetchrow;
		$sth_highnr->finish;

		$record->{deelnr} = $TMPARR[0] + 1;
	}
	
	$statmesg = sprintf( "Wegschrijven record. Momentje" ); $mw->update;


	foreach (@velddef)
	{
		push( @fmtfields, "$_->{key}=" . $dbh->quote($record->{$_->{key}}) );
	}

	if ( $deelnr == 0 )
	{
		$cmd = "INSERT zwemmers SET " . join( ", ", @fmtfields );
	} else {$cmd = "UPDATE zwemmers SET " . join( ", ", @fmtfields ) . "WHERE deelnr=$deelnr"; }

	$sth = $dbh->prepare( $cmd );
	if ($sth->execute) {
	$statmesg = sprintf( "Record opgeslagen" );
		return 1;
	} else {
		$statmesg = sprintf("Probleem met opslaan record"); 
		$record->{deelnr} =$deelnr; 
		$mw->update;
	}
		
}; #end sub write_record






sub printkaart ($)
{
	my ($top, $record) = @_;
	my $nrofmedals = " ";
	
	if ( $record->{medalsrt} eq "Normaal" )
	{
		$nrofmedals = sprintf( "%d", $record->{medail} );
	} else {
		$nrofmedals = "Pierebad";
	}
	
################################################################################################
# Formaat voor de deelnemerskaart
format KAART =
























                 Deelnemer @<<<<  Te behalen medaille: @<<<<<
$record->{deelnr}, $nrofmedals
                 @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
$record->{naam}
                 @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
$record->{adres}
                 @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
$record->{woonpl}
                 Geplande zwemtijd: @<<<<<<
$record->{tydhhmm}
                 Printdatum: @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
$today1
.

	open( KAART, ">:utf8","kaart$myuserid.txt" ) or die "Can't open printer 'lpt1': $!";
	write KAART;
	print KAART "\f";
	close KAART;

	my $kaart=read_text("kaart$myuserid.txt");
	$kaart =~ s/^\s+|\s+$//g;
	my $pdf = PDF::Create->new(
		'filename' => "kaart$myuserid.pdf",
		"Author" => "Wattem",
		"Title" => "Kaart deelnemer ".$record->{deelnr},
		"CreationDate"=> [ localtime ]
	);
	my $root=$pdf->new_page("MediaBox"=>$pdf->get_page_size('A5'));
	my $page=$root->new_page;
	my $font=$pdf->font('BaseFont' => 'Helvetica');
	my $toc = $pdf->new_outline('Title' => "Kaart deelnemer ".$record->{deelnr}, 'Destination' => $page);
	$page->block_text({
		page => $page,
		font => $font,
		text => $kaart,
		font_size => 10,
		text_color => [0,0,0],
		line_width => 400,
		start_y => 500,
		end_y => 10,
		'x' => 10,
		'y' => 10
	});
	$pdf->close;
	my $result=$lineprinter->printfile("kaart$myuserid.txt") or die $lineprinter->printerror();
	

	sluit_window( $top, $record );
}; # end sub printkaart






sub count_medals ()
{
	my $top;
	my $cmd;
	my $sth_medals;
	my %medals;
		
	$cmd = 'SELECT medalsrt,0 + medail AS medalcnt,COUNT(*) AS aantal FROM zwemmers ';
	$cmd = sprintf( "%s WHERE AANGEMELD LIKE '%s%%'",
			$cmd, $current_year );
	$cmd .= 'GROUP BY medalsrt,medail ORDER BY medalsrt,medalcnt,aantal DESC;';
	if ( ! ($sth_medals = $dbh->prepare_cached( "$cmd" ) ) )
	{	$statmesg = sprintf("Kan geen medaillesoorten bepalen???");
		printf( "Error prepare medals: %s\n", $dbh->errstr );
		return 1;
	}

	$top = $mw->Toplevel();
	$top->waitVisibility;
	$top->title( "Overzicht medailles" );
		
	$top->Label
		( -textvariable	=> \$statmesg
		)->pack	( -side		=> 'top'
			, -fill		=> 'x'
			, -expand	=> 0
			);
	$statmesg = sprintf( 'Overzicht samenstellen' );
	$mw->update;

	my $listbox = $top->Scrolled
		( 'Listbox'
		, -scrollbars	=> 'osoe'
		, -font		=> 'courier 10'
		, -width	=> 25
		)->pack	( -fill		=> 'both'
			, -expand	=> 1
			);
			
	$sth_medals->execute() or die "Kan overzicht medailles niet samenstellen";
	
	my $regel = sprintf( "%-10s %6s %6s",
			'Soort', 'Aantal', 'Aantal' );
	$listbox->insert( 'end', $regel );
	$regel = sprintf( "%-10s %6s %6s",
			'medaille', 'jaar', 'stuks' );
	$listbox->insert( 'end', $regel );
	$regel = sprintf( "%-10s %6s %6s",
			'----------', '------', '------' );
	$listbox->insert( 'end', $regel );
	while ( my @veld = $sth_medals->fetchrow )
	{
		$regel = sprintf( "%-10s %6d %6d", $veld[0], $veld[1], $veld[2] );
		$listbox->insert( 'end', $regel );
		my $key = sprintf( "%s %02d", $veld[0], $veld[1] );
		$medals{$key} += $veld[2];
	}
	$sth_medals->finish;
	$top->Button
		( -text		=> 'Sluit'
		, -command	=> sub { destroy $top; }
		)->pack
			( -side	=> 'left'
			, -fill	=> 'both'
			);
	$top->Button
		( -text		=> 'Print'
		, -command	=> sub { print_medals( \%medals ); }
		)->pack
			( -side	=> 'left'
			, -fill	=> 'both'
			);
	$statmesg = "";
}; # end sub count_medals





sub print_medals ($)
{
	my ($r_medals) = @_;
	my $lyst;
	my $regel;
	my $lastsoort;
	
	format MEDALS =
Benodigd aantal medailles in @<<<<
$current_year
---------------------------------
soort     aantal  benodigd
medaille  jaren   aantal
--------------------------
@*
$lyst
---------------------------------
.

	$lastsoort = '';
	foreach my $key ( sort keys %$r_medals )
	{
		my ($soort,$keer) = split( '\s+', $key );
		if ( $soort ne $lastsoort )
		{
			$lastsoort = $soort;
			$regel = sprintf( "%-10s", $soort );
		} else {$regel = sprintf( "%-10s", '' );
		}
		$regel = sprintf( "%s %6d %6d", $regel, $keer, $r_medals->{$key} );
		$lyst .= $regel . "\n";
	}

	open( MEDALS, ">lpt1" ) or die "Can't open printer 'lpt1'";
	write MEDALS;
	print MEDALS "\f";
	close MEDALS;
}; #end sub print_medals






sub count_locats ()
{
	my $top;	
	my $cmd;
	my $sth_woonpl;
	my %locats;
	
	$cmd = 'SELECT woonpl, count(*) AS aantal FROM zwemmers';
	$cmd = sprintf( "%s WHERE AANGEMELD LIKE '%s%%'",
			$cmd, $current_year );
	$cmd .= 'GROUP BY woonpl ORDER BY aantal desc, `woonpl` ASC';
	if ( ! ($sth_woonpl = $dbh->prepare_cached( "$cmd" ) ) )
	{	$statmesg = sprintf("Kan geen lokaties bepalen???");
		printf( "Error prepare medals: %s\n", $dbh->errstr );
		return 1;
	}
	$top = $mw->Toplevel();
	$top->waitVisibility;
	$top->title( "Overzicht lokaties" );
	
	$top->Label
		( -textvariable	=> \$statmesg
		)->pack	( -side		=> 'top'
			, -fill		=> 'x'
			, -expand	=> 0
			);
	$statmesg = sprintf( 'Overzicht samenstellen' );
	$mw->update;

	my $listbox = $top->Scrolled
		( 'Listbox'
		, -scrollbars	=> 'osoe'
		, -font		=> 'courier 10'
		, -width	=> 30
		)->pack	( -fill		=> 'both'
			, -expand	=> 1
			);
			
	$sth_woonpl->execute() or die "Kan overzicht lokaties niet samenstellen";
	
	my $regel = sprintf( "%-25s%4s", "Lokatie", "Aantal" );
	$listbox->insert( 'end', $regel );
	my $regel = sprintf( "%-25s%6s", "-------", "------" );
	$listbox->insert( 'end', $regel );
	
	while ( my @veld = $sth_woonpl->fetchrow )
	{
		$regel = sprintf( "%-25s%4d", $veld[0], $veld[1] );
		$locats{$veld[0]} = $veld[1];
		$listbox->insert( 'end', $regel );
	}
	$sth_woonpl->finish;
	
	$top->Button
		( -text		=> 'Sluit'
		, -command	=> sub { destroy $top; }
		)->pack
			( -side	=> 'left'
			, -fill	=> 'both'
			);
	$top->Button
		( -text		=> 'Print'
		, -command	=> sub { print_locats( \%locats ); }
		)->pack
			( -side	=> 'left'
			, -fill	=> 'both'
			);
	$statmesg = "";
}; # end sub count_locats





sub print_locats ($)
{
	my ($r_locats) = @_;
	my $lyst;
	my $regel;
	
	format LOCATS =
Lokaties in @<<<<
$current_year
----------------------------
Lokatie               Aantal
--------------------  ------
@*
$lyst
----------------------------
.

	foreach my $key ( sort keys %$r_locats )
	{
		$regel = sprintf( "%-20s: %6d", $key, $r_locats->{$key} );
		$lyst .= $regel . "\n";
	}

	open( LOCATS, ">lpt1" ) or die "Can't open printer 'lpt1'";
	write LOCATS;
	print LOCATS "\f";
	close LOCATS;
}; #end sub print_locats






###############################################################################################
# main program
###############################################################################################
#
# Opbouw scherm:
#
#   -------------------------------------------------------------------------------------
#  | $mw = Main window                                                                   |
#  |                                                                                     |
#  |   -------------------------------------------------------------------------------   |
#  |  | $menwin = Frame t.b.v. menus                                                  |  |
#  |   -------------------------------------------------------------------------------   |
#  |                                                                                     |
#  |   -------------------------------------------------------------------------------   |
#  |  | $msgwin = Frame t.b.v. messages                                               |  |
#  |  |                                                                               |  |
#  |   -------------------------------------------------------------------------------   |
#  |                                                                                     |
#  |   -------------------------------------------------------------------------------   |
#  |  | $mainfm = Frame t.b.v. het eigenlijke werk                                    |  |
#  |  |                                                                               |  |
#  |  |                                                                               |  |
#  |   -------------------------------------------------------------------------------   |
#  |                                                                                     |
#  |                                                                                     |
#   -------------------------------------------------------------------------------------

# Define the main window
$mw = MainWindow->new;

$mw->title( "Zoekscherm" );
# Define the three inner windows.
my $menwin = $mw->Frame
		 ( -relief	=> 'raised'
		 , -borderwidth	=> 2
		 )-> pack
		 	( -side	=> "top"
		 	, -fill	=> "x"
		 	);
my $msgwin = $mw->Frame
		 ( -relief	=> 'raised'
		 , -borderwidth	=> 2
		 )-> pack
		 	( -side		=> "top"
		 	, -fill		=> "x"
		 	, -expand	=> 1
		 	);
my $mainfm = $mw->Frame
		 ( -relief	=> 'raised'
		 , -borderwidth	=> 2
		 )-> pack
		 	( -side		=> "top"
		 	, -fill		=> 'both'
		 	, -expand	=> 1
		 	);

# Now fill the message-window. It will be used to display all sorts of messages.
$msgwin->Label
		( -textvariable	=> \$statmesg
        	) -> pack
        		( -side		=> "left"
        		, -fill		=> "x"
        		, -expand	=> 1
        		);

# Fill the menu
my $submenu = $menwin->Menubutton
	( -text		=> "Beheer"
	, -tearoff	=> 0
	, -anchor	=> "w"
	)->pack	( -side	=> 'left'
		, -fill	=> 'both'
		);
$submenu->AddItems
	( [ 'command'	=> "Exit"
	  , -command	=> sub { destroy $mw; } ]
	);

$submenu = $menwin->Menubutton
	( -text		=> "Overzichten"
	, -tearoff	=> 0
	, -anchor	=> 'w'
	)->pack	( -side	=> 'left'
		, -fill	=> 'both'
		);

$submenu->AddItems
	( [ 'command'	=> 'Medailles'
	  , -command	=> \&count_medals
	  ]
	, [ 'command'	=> 'Lokaties'
	  , -command	=> \&count_locats
	  ]
	);

init_db();
create_search( $mainfm );

check4bewerk();
MainLoop;
