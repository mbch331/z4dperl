﻿@velddef = (	{ 'key'		=> 'deelnr'
		, 'txt'		=> 'Deelnemernr'
		, 'activ'	=> '0'
		, 'check'	=> \&chk_deelnemernr
		}
	   ,	{ 'key'		=> 'naam'
		, 'txt'		=> 'Naam'
		, 'activ'	=> '1'
		, 'check'	=> \&chk_naam
		}
	   ,	{ 'key'		=> 'adres'
		, 'txt'		=> 'Adres'
		, 'activ'	=> '1'
		, 'check'	=> \&chk_adres
		}
	   ,	{ 'key'		=> 'postcode'
		, 'txt'		=> 'Postcode'
		, 'activ'	=> '1'
		, 'check'	=> \&chk_post
		}
	   ,	{ 'key'		=> 'woonpl'
		, 'txt'		=> 'Woonplaats'
		, 'activ'	=> '1'
		, 'check'	=> \&chk_woonpl
		}
	   ,	{ 'key'		=> 'land'
		, 'txt'		=> 'Land'
		, 'activ'	=> '1'
		, 'check'	=> \&chk_land
		}
	   ,	{ 'key'		=> 'telf'
		, 'txt'		=> 'Telefoon'
		, 'activ'	=> '1'
		, 'check'	=> \&chk_telf
		}
	   ,    { 'key'         => 'mail'
		, 'txt'         => 'Emailadres'
		, 'activ'       => '1'
		, 'check'       => \&chk_mail
		}
	   ,	{ 'key'		=> 'birth'
		, 'txt'		=> 'Geboortedatum'
		, 'activ'	=> '1'
		, 'check'	=> \&chk_birth
		}
	   ,	{ 'key'		=> 'aangemeld'
		, 'txt'		=> 'Aangemeld d.d.'
		, 'activ'	=> '0'
		, 'check'	=> \&chk_aangemeld
		}
	   ,	{ 'key'		=> 'medail'
		, 'txt'		=> 'Aantal medailles'
		, 'activ'	=> '1'
		, 'check'	=> \&chk_medals
		}
	   ,	{ 'key'		=> 'medalsrt'
		, 'txt'		=> 'Soort medaille'
		, 'ckbtn'	=> '1'
		, 'txtrue'	=> 'Pierebad'
		, 'txfalse'	=> 'Normaal'
		, 'activ'	=> '1'
		, 'check'	=> \&chk_srtmdl
		}
	   ,	{ 'key'		=> 'tydhhmm'
		, 'txt'		=> 'Planning tijd'
		, 'activ'	=> '0'
		, 'check'	=> \&chk_dummy
		}
	   ,	{ 'key'		=> 'bewerk'
		, 'txt'		=> 'In bewerking'
		, 'activ'	=> '0'
		, 'check'	=> \&chk_dummy
		}
	   );# End definitie @velddef





sub disperr
{	# Parameters: $message
	# uses global variables: $mw
	
	my $msg = shift;
	my $errwin = $mw->Toplevel();
	$errwin->Label
		(	-text	=> $msg
		) -> pack
			(	-side	=> "top"
			,	-fill	=> "x"
			);
	$errwin->Button
		(	-text		=> "Quit"
		,	-command	=> sub { $mw->destroy; }
		) -> pack
			(	-side	=> "top"
			,	-fill	=> "both"
			);
	$errwin->grab(); $errwin->focus();
	MainLoop;
}; #end sub disperr





sub tolower
{
	my @out = @_;
	for (@out)
	{
		tr/A-Z/a-z/;
	}
	return wantarray ? @out : $out[0];
}; # endsub tolower





sub str2dattim
{	# use globals:
	# Converteert een datum-tijdstring 'YY-MM-DD HH:MM'
	# naar twee strings: YY-MM-DD en HH:MM
	# Eventuele derde parameters (bijv het aantal uit de tijdenfile) worden ook terug
	# gegeven. Tijdenfile bevat records met syntax '00-05-31 15:20 20'
	# Er kan dus gebruik gemaakt worden van het commando:
	# ($YYMMDD, $HHMM, $AANTAL) = dattim2str( $INPUT );
	
	return ( split( "\b+", $_[0] ) );
}; # endsub str2dattim





sub dattim2str
{	# use globals:
	# Converteert een array met een datum en een tijd (YY-MM-DD, HH:MM) naar een string

	return( sprintf( "%s %s", $_[0], $_[1] ) );
}; # endsub dattim2str





sub trim
{
	my @out = @_;
	for (@out)
	{
		s/^\s+//;
		s/\s+$//;
	}
	return wantarray ? @out : $out[0];
}; # end sub trim




sub chk_alfanum($)
{ # General check-function
  my ($string) = @_;
  

	# Controleer of veld alfanumeriek is
	# Dus het mag letters (inclusief umlauts (o"=?,), cijfers en '"-&*()_=+/ .,? bevatten
	return ( $string =~ /^[\p{L}0-9\'\"\-\&\*\(\)\_\=\+\/ \.\,\?]*$/ );
}; # end sub chk_alfanum


	


sub chk_datum ($)
{ # General check-function
  # Geeft terug:
  # 	0 -> fout
  # 	geformateerde string

  # Controleer een ingegeven datum
  # 	Mogelijke formaten:		Bijv:
  #  1	YYYY-MM-DD	 2 DD-MM-YYYY	1973-05-08	08-05-1973
  #  3	YYYY-M-DD	 4 DD-M-YYYY	1973-5-08	08-5-1973
  #  5	YYYY-MM-D	 6 D-MM-YYYY	1973-05-8	8-05-1973
  #  7	YYYY-M-D	 8 D-M-YYYY	1973-5-8	8-5-1973
  #  9	  YY-MM-DD	10 DD-MM-YY	  73-05-08	08-05-73
  # 11	  YY-M-DD	12 DD-M-YY	  73-5-08	08-5-73
  # 13	YY-MM-D		14 D-MM-YY	  73-05-8	8-5-73
  # 15	  YY-M-D	16 D-M-YY	  73-5-8	8-5-73
  # 17	YYYYMMDD	18 DDMMYYYY	19730508	08051973
  # 19	  YYMMDD	20 DDMMYY	  730508	080573
  #
  # ipv een minteken (-) kan ook een slash (/) worden gebruikt.
	my ($field) = @_;

	my @arr = ();
	my $dat_ct = 0; # Century
	my $dat_yy = 0;
	my $dat_mm = 0;
	my $dat_dd = 0;

	# Eerst leading/trailing spaces verwijderen
	$field = trim ( $field );

	# Nu de slash vervangen door een minteken
	$field =~ s#/#-#g;

	# Stoppen als invoer iets anders bevat als cijfers en mintekens
	if ( $field =~ /[^0-9\-]/ )
	{
		return 0;
	}
	my $n_kars = length( $field );

	# Invoer opsplitsen in jaar, maand en dag indien mogelijk
	(@arr) = split( '-', $field );
	# @arr bestaat nu uit 1 of 3 elementen. Zoniet, dan is de syntax fout
	if ( @arr == 1 )
	{ # Mogelijkheden 17 t/m 20
	  # 17	YYYYMMDD	18 DDMMYYYY	19730508	08051973
	  # 19	  YYMMDD	20 DDMMYY	  730508	080573
		if ( $n_kars == 6 )
		{	# 6 karakters: mogelijkheden 19 en 20
			# 19	  YYMMDD	20 DDMMYY	  730508	080573
			if ( $arr[0] =~ /([0-9][0-9])([0-9][0-9])([0-9][0-9])/ )
			{	# $1 zijn de eerste twee karakters
				# $2 zijn de volgende twee
				# $3 zijn de laatste twee
			  	$dat_yy = $1;
			  	$dat_mm = $2;
			  	$dat_dd = $3;
			  	if ( $dat_dd > 31 || $dat_dd == 0 )
			  	{ # YY en DD omgewisseld?
			  		($dat_dd, $dat_yy) = ($dat_yy, $dat_dd);
			  	} elsif ( $dat_yy > 0 && $dat_yy <= 31 )
			  	{	# zowel YY als DD is een getal tussen 1 en 31
			  		# Dus is het niet duidelijk welke DD en welke
			  		# YY is.
			  		return 0;
			  	}
				$dat_yy += 2000;
				if ( $dat_yy > $current_year )
				{ $dat_yy -= 100;
				}
			} else { # Geen 6 cijfers? dan fout
				return 0;
			}
		} elsif ( $n_kars == 8 )
		{	# 8 karakters: mogelijkheden 17 en 18
			# 17	YYYYMMDD	18 DDMMYYYY	19730508	08051973
			if ( $arr[0] =~ /([0-9][0-9])([0-9][0-9])([0-9][0-9])([0-9][0-9])/ )
			{	# $1 $2 $3 en $4 bevatten allen 2 cijfers
				if ( $3 > 12 )
				{	# Dus laatste gedeelte = YYYY (19xx of 20xx)
					($dat_dd, $dat_mm, $dat_ct, $dat_yy) = ($1,$2,$3,$4);
				} else {
					($dat_ct, $dat_yy, $dat_mm, $dat_dd) = ($1,$2,$3,$4);
				}
				$dat_yy += $dat_ct * 100;
			} else { # geen 8 cijfers, dan fout
				return 0;
			}
		} else { # Geen 6 of 8 karakters, dus fout
			return 0;
		}
	} elsif ( @arr == 3 )
	{	# meerdere elementen in @arr: Mogelijkheden 1 t/m 16
		#  1	YYYY-MM-DD	 2 DD-MM-YYYY	1973-05-08	08-05-1973
		#  3	YYYY-M-DD	 4 DD-M-YYYY	1973-5-08	08-5-1973
		#  5	YYYY-MM-D	 6  D-MM-YYYY	1973-05-8	8-05-1973
		#  7	YYYY-M-D	 8  D-M-YYYY	1973-5-8	8-5-1973
		#  9	  YY-MM-DD	10 DD-MM-YY	  73-05-08	08-05-73
		# 11	  YY-M-DD	12 DD-M-YY	  73-5-08	08-5-73
		# 13	  YY-MM-D	14  D-MM-YY	  73-05-8	8-5-73
		# 15	  YY-M-D	16  D-M-YY	  73-5-8	8-5-73

		if ( grep( /\D/, @arr  ) )
		{	# 1 of meer elementen terug, dan zijn er niet-digits gebruikt: error
			return 0;
		}
		# Alleen maar digits.
		if ( $arr[2] == 0 || $arr[2] > 31 )
		{	# 3e element is 0 (dus jaar 2000) of groter dan 31 (dus geen DD)
			# 
			# Even switchen
			($arr[0], $arr[2]) = ($arr[2], $arr[0]);
		}
		# Nu zijn mogelijkheden 2, 4, 6 en 8 omgezet naar mogelijkheden 1, 3, 5 en 7
		if ( $arr[0] =~ /\d\d\d\d/ )
		{	# 1e element = 4 digits: mogelijkheden 1, 3, 5, 7
			if ( $arr[0] < 1900 || $arr[0] > $current_year )
			{
				return 0;
			}
			# YYYY is goed bevonden.
			($dat_yy, $dat_mm, $dat_dd) = @arr;
		} else { # Mogelijkheden 9 t/m 16
			#  9	  YY-MM-DD	10 DD-MM-YY	  73-05-08	08-05-73
			# 11	  YY-M-DD	12 DD-M-YY	  73-5-08	08-5-73
			# 13	  YY-MM-D	14  D-MM-YY	  73-05-8	8-5-73
			# 15	  YY-M-D	16  D-M-YY	  73-5-8	8-5-73

			# Als YY groter is dan 31 of gelijk is aan 0, dan weten we zeker dat we
			# het jaarveld weten
			if ( $arr[0] > 31 || $arr[0] == 0)
			{
				($dat_yy, $dat_mm, $dat_dd) = @arr;
				$dat_yy += 2000;
				if ( $dat_yy > $current_year )
				{ $dat_yy -= 100;
				}
			} elsif ( $arr[2] > 31 )
			{
				($dat_dd, $dat_mm, $dat_yy) = @arr;
				$dat_yy += 2000;
				if ( $dat_yy > $current_year )
				{ $dat_yy -= 100;
				}
			} else
			{	# We weten niet welk veld de DD is en welke de YY
				return 0;
			}
		};
	} else { # Niet 1 of 3 elementen: foute syntax
		return 0;
	}; # end if ( @arr == 1 )
	(@arr) = ($dat_yy, $dat_mm, $dat_dd);
	$field = join( '-', $dat_yy, sprintf('%02d',$dat_mm), sprintf('%02d',$dat_dd) );
	return $field;
}; # end sub chk_datum





sub chk_deelnemernr ($)
{ # Deelnemernr moet numeriek zijn, maar bij nieuwe records mag het leeg zijn.
	my ($record) = @_;
	# Eerst leading/trailing spaces verwijderen
	$record->{deelnr} = trim ( $record->{deelnr} );
	if ($record->{deelnr} =~ /^\d*$/ )
	{
		$record->{deelnr} = sprintf( "%d", $record->{deelnr} );
		return 1;
	} else {return 0;}
}; #end sub chk_deelnemernr





sub chk_naam ($)
{ # Naam moet alfanumeriek zijn en mag niet leeg zijn
	my ($record) = @_;
	$record->{naam} = trim( $record->{naam} );
	return ( $record->{naam} && chk_alfanum( $record->{naam} ) );
}; # end sub chk_naam





sub chk_adres ($)
{ # Adres moet alfanumeriek zijn en mag niet leeg zijn
	my ($record) = @_;
	$record->{adres} = trim( $record->{adres} );
	return ( $record->{adres} && chk_alfanum( $record->{adres} ) );
}; # end sub chk_adres




sub chk_post ($)
{ # Postcode = leeg of 4 digits en 2 letters met 0 of meer spaties er tussen
	my ($record) = @_;
	$record->{postcode} = trim( $record->{postcode} );

	# Vervang eerst meerdere spaties door een spatie
	$record->{postcode} =~ s/  */ /g;
	if ( $record->{postcode} =~ /^$/ )
	{ # OK. Postcode mag leeg zijn
		return 1;
	}
	elsif ($record->{postcode} =~/^([0-9][0-9][0-9][0-9])$/ && $record->{land} =='België')
	{ # OK. België kent alleen cijfers
		return 1;	
	}
	elsif ( $record->{postcode} =~ /^([0-9][0-9][0-9][0-9]) ?([A-Za-z][A-Za-z])$/ )
	{ # Postcode OK, nu maak er 4 cijfers, een spatie en 2 hoofdletters van
		$record->{postcode} = sprintf( "%s %s", $1, $2 );
		$record->{postcode} =~ tr/a-z/A-Z/;
		return 1;
	} else { # Foutieve postcode
		return 0;
	}
}; # end sub chk_post




sub chk_woonpl ($)
{ # Woonplaats moet alfanumeriek zijn en mag niet leeg zijn
	my ($record) = @_;
	$record->{woonpl} = trim( $record->{woonpl} );
	return ( $record->{woonpl} && chk_alfanum( $record->{woonpl} ) );
}; # end sub chk_woonpl

sub chk_land ($)
{ # Woonplaats moet alfanumeriek zijn en mag niet leeg zijn
	my ($record) = @_;
	$record->{land} = trim( $record->{land} );
	return ( $record->{land} && chk_alfanum( $record->{land} ) );
}; # end sub chk_woonpl



sub chk_telf( $ )
{ # Telefoon mag alleen maar cijfers, spaties en '-' bevatten
  # Formaat: '040-9999999' of '040 9999999' of '9999999'
  # Dus: alleen maar cijfers: dan ook precies 7 stuks. Er wordt dan 040 voor gezet
  # of beginnend met een 0, daarna twee of meer cijfers,
  # gevolgd door een spatie of een min-teken (spatie wordt vervangen door -)
  # gevolgd door nog een aantal cijfers.
  # Aantal cijfers moet in totaal 10 zijn
	my ($record) = @_;
	$record->{telf} = trim( $record->{telf} );
	
	# Telefoon mag leeg zijn.
	if ( $record->{telf} =~ /^$/ )
	{	return 1;
	}

	if ( $record->{telf} =~ /^\d*$/ && length($record->{telf}) == 7 )
	{ # Local telephone: add 040- to the front
		$record->{telf} = sprintf( "040-%s", $record->{telf} );
		return 1;
	}
	
#	if ( $record->{telf} =~ /^\d*$/ && length($record->{telf}) == 10)
#	{ # Telefoonnummer mag 10 cijfers zijn
#		$record->{telf} = sprintf( "%s
#	}

	if ( $record->{telf} =~ /^(0[1-9][0-9]*)[ -]([0-9]*)$/ )
	{ # Telefoonnummer inclusief netnummer
		my $temp = sprintf( "%s%s", $1, $2 );
		if ( length( $temp ) != 10 )
		{
			return 0;
		} else {
			$record->{telf} =~ s/ /-/;
			return 1;
		}
	}
	if ($record->{telf} =~ /^(00324[0-9]{8})$/ )
	{
		return 1;
	}
	if($record->{telf} =~ /^(0032[1-35-9][0-9]{6,7})$/)
	{
		return 1;
	}
	if ( $record->{telf} =~ /^(06)-?([1-9][0-9]{7})$/ ) 
	{
		$record->{telf} = sprintf("%s-%s",$1,$2);
		return 1;
	}
	return 0;	
}; # end sub chk_telf





sub chk_birth ($)
{
	my ($record) = @_;
	# Leeg veld wordt toegestaan
	if ( $record->{birth} =~ /^$/ )
	{
		return 1;
	}

	my $birth = chk_datum( $record->{birth} );
	if ( $birth )
	{
		$record->{birth} = $birth;
	}
	return ( $birth );
}; # end sub chk_birth




sub chk_aangemeld ($)
{ # Hierin moet een datum staan (YYYYMMDD)
  # Het wordt door het systeem gegenereerd, maar natuurlijk zou het mogelijk kunnen zijn,
  # dat iemand de database heeft ge-edit.
	my ($record) = @_;
	$record->{aangemeld} = trim( $record->{aangemeld} );

	return ($record->{aangemeld} =~ /^\d*$/ );
}; #end sub chk_aangemeld





sub chk_medals ($)
{ # Numeriek, mag leeg zijn
	my ($record) = @_;
	$record->{medail} = trim( $record->{medail} );

	return ($record->{medail} =~ /^\d*$/ );
}; # end sub chk_medals




sub chk_srtmdl ($)
{	# use globals:
	# Checked op soort medaille
	my ($record) = @_;
#		, 'txtrue'	=> 'Pierebad'
#		, 'txfalse'	=> 'Normaal'

	return 1;
}; #endsub chk_srtmdl




sub chk_dummy ($)
{	my ($record) = @_;
	return 1;
}; # endsub chk_dummy


sub chk_mail ($)
{	my ($record) = @_;
	$record->{mail} = trim( $record->{mail} );
	return 1;
}; # endsub chk_dummy


sub disp_record ($$$@)
{
#	Function: creeer een nieuw frame met de opgegeven title
#			en opgegeven velden
#
#	parameters:
#		$1 = $frame waar window in moet komen
#		$2 = pointer naar hash met velden
#		$3 = Mag record gewijzigd worden?
#			0 = nee
#			1 = ja, alleen wijzigbare velden (dus niet bijv. deelnemernr)
#			2 = alle velden
#		$4 = Zoekbutton. Aktiveren indien veld 3 = 2

	my ($frame, $record, $editable, $zoekbutton) = @_;
	
	# uses global variables: @velddef
	# velddef is een array met pointers naar de velddefinities
	# dus $velddef->key = key in database
	#  en $velddef->txt = de tekst die op het scherm moet komen
	#  enz.
	#

	# uses local variables:
	# $firstfield = eerste veld dat gewijzigd mag worden
	my $firstfield = '';
	my $lastfield = '';
	
	
	# Scherm opbouwen	
	foreach (@velddef)
	{
		# Maak een frame voor deze regel
		my $fmrgl = $frame->Frame()->pack
			( -side		=> 'top'
			, -fill		=> 'x'
			, -expand	=> 1
			);
		$fmrgl->Label
			( -text		=> $_->{txt}
			, -width	=> 20
			, -anchor	=> 'w'
			)->pack( -side	=> 'left' );
		if ( defined $_->{ckbtn} )
		{ # checkbutton op scherm zetten
#########################################################################################
# Enkel voor zwemvierdaagse:
# De enige checkbutton is medalsrt
# Als deze wijzigt, dan moet het aantal medailles op 0 worden gezet
# en tydhhmm (geplande zwemtijd) op leeg.
#########################################################################################
			$fld = $fmrgl->Checkbutton
				( -variable	=> \$record->{$_->{key}}
				, -onvalue	=> $_->{txtrue}
				, -offvalue	=> $_->{txfalse}
				, -textvariable	=> \$record->{$_->{key}}
				, -command	=>
					sub	{	$record->{tydhhmm} = '';
							$record->{medail} = 0;
							$record->{aangemeld} = '';
						}
				)->pack
					( -fill		=> 'none'
					, -expand	=> 0
					, -anchor	=> 'w'
					);
		} else {
			$fld = $fmrgl->Entry
				( -textvariable	=> \$record->{$_->{key}}
				)->pack
					( -fill		=> 'x'
					, -expand	=> 1
					);
		}
		
		if ( ! $editable || ( $editable == 1 && ! $_->{activ} ) )
		{	# Als record niet gewijzigd mag worden
			# of het veld is een niet aktief veld (bijv. DEELNR)
			# Dan wordt dit veld gedisabled
			$fld->configure
				( -state	=> 'disabled'
				, -relief	=> 'groove'
				);
		} else {
			$lastfield = $fld;
			if ( $firstfield eq "" )
			{
				$firstfield = $fld;
			}
		}
		
		# Verbind ENTER in het huidige veld met het geven van de focus
		# aan het volgende veld in het frame wat wordt aangegeven door
		# de eerste parameter
		if ( $editable == 2 && $zoekbutton )
		{
			$fld->bind
			( '<Return>' => sub
				{ $zoekbutton->invoke();
				}
			)
		} else {
			$fld->bind
			( '<Return>' => sub
				{ $_[0]->focusNext();
				}
			)
		}
	# Opvangen control-keys:
	#	' = quoteright
	# sh' =	" = quotedbl
	#	` = quoteleft
	# sh` =	~ = asciitilde
	# 	; = semicolon
	# sh; =	: = colon
	#
	$fld->bind( "<Control-quoteright>", [ \&quoted, "'" ] );
	$fld->bind( "<Control-quotedbl>",   [ \&quoted, '"' ] );
	$fld->bind( "<Control-quoteleft>",  [ \&quoted, '`' ] );
	$fld->bind( "<Control-asciitilde>", [ \&quoted, '~' ] );
	$fld->bind( "<Control-colon>",      [ \&quoted, ':' ] );
	$fld->bind( "<Control-semicolon>",  [ \&quoted, ';' ] );
	}; # end for each @velddef
	
	if ( $lastfield ne ""  && $firstfield ne "" )
	{	# Verbind ENTER in laatste veld met focus op eerste veld
		$lastfield->bind
			( '<Return>' => sub
				{ $firstfield->focus() }
			);
	}
#	printf( "Einde sub disp_record\n" );
}; # end sub disp_record





sub quoted
{	# Vang Control + '" ;: en `~ op
	# Vervang karakters aeiou door ????? ????? ?????
	my ($entry, $kar ) = @_;
	
	my $realstr = $entry->cget( '-textvariable' );
	my $curpos = $entry->index('insert');
	if ( $curpos )
	{
		if ( $kar eq '"' || $kar eq "'" )
		{
			substr( $$realstr, $curpos - 1, 1) =~ tr/aeiou/?????/;
		} elsif ( $kar eq ';' || $kar eq ':' )
		{
			substr( $$realstr, $curpos - 1, 1 ) =~ tr/aeiou/?????/;			
		} elsif ( $kar eq '`' || $kar eq '~' )
		{
			substr( $$realstr, $curpos - 1, 1 ) =~ tr/aeiou/?????/;	
		}		
	}
}; # end sub quoted






1;
